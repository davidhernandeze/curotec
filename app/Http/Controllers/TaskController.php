<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index() {
        return Task::all();
    }

    public function store(Request $request) {
        $task = new Task;
        $task->value = $request['value'];
        $task->is_completed = false;
        $task->save();

        return response([], 200);
    }

    public function toggleCompleted(Task $task) {
        $task->is_completed = ! $task->is_completed;
        $task->save();

        return response([], 200);
    }

    public function destroy(Task $task) {
        $task->delete();

        return response([], 200);
    }
}
